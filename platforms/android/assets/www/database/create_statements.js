// Create all tables
function createAllTables(){
     createTableProjects();
     createTableSchedule();
     createTableCountTimes();
     createTableSpecies();
     createTableProjectSpecies();
     createTableHeader();
     createTableCounts();
     createTableParties();
     createTableAnglers();
     createTableFishCaught();
     createTableProjectsValidate();
 }

// Dynamic Function to create table
function createTable(tableName, columns) {
    myDB.transaction(function(transaction) {
        // Create column arrays
        var columnNames = [];
        for (var i=0; i<columns.length; i++){
            columnNames[i] = columns[i];
        }
        // Produce create query
        var columnNamesQuery = "";
        for (var i=0; i<columnNames.length; i++){
            if(i==0){
                columnNamesQuery = columnNamesQuery + columnNames[i];
            } else{
                columnNamesQuery = columnNamesQuery + ", " + columnNames[i];
            }
        }
        // Create query
        var query = 'CREATE TABLE IF NOT EXISTS ' + tableName + ' (' + columnNamesQuery + ')';
        transaction.executeSql(query, [],
        function(tx, result) {
            if(tableName == "tblparties"){
                // navAlert("Table " + tableName + " created successfully");
            }
        },
        function(error) {
             navAlert("Error occurred while creating table:" + JSON.stringify(error));
        });
    });
}

// Function Lists
function createTableProjects() {
    var columnNames = ["projectUID text PRIMARY KEY","projectName text","projectType text","projectLocation text","projectDescription text","projectStartDate text","projectStopDate text","optionalQ1 text","optionalQ2 text","optionalQ3 text","optionalQ4 text","optionalQ5 text","optionalQ6 text"];
    createTable("tblprojects",columnNames);
}

function createTableSchedule() {
    var columnNames = ["scheduleUID text primary key","projectUID text","surveyStartDateTime text","surveyStopDateTime text","surveyLocation text","surveyStatus text"];
    createTable("tblschedule",columnNames);
}

function createTableCountTimes() {
    var columnNames = ["countTimesUID text primary key","scheduleUID text","projectUID text","countStartDateTime text","countStopDateTime text"];
    createTable("tblcounttimes",columnNames);
}

function createTableSpecies() {
    var columnNames = ["speciesCode text primary key","speciesCommonName text","speciesLatinName text"];
    createTable("tblspecies",columnNames);
}

function createTableProjectSpecies() {
    var columnNames = ["projectUID text","speciesCode text","speciesNameFirst text","speciesNameLast text","inList text","inLargeList text","inAnglerPref text","warnMinSize text","warnMaxSize text","PRIMARY KEY (projectUID, speciesCode)"];
    createTable("tblprojectspecies",columnNames);
}

function createTableHeader() {
    var columnNames = ["headerUID text primary key","scheduleUID text","projectUID text","clerk text","secchi text","waterTemperature integer","waterLevel text","clerkComments text","headerStatus text","machineID text","dateCreated text","dateModified text","gps text","uploadStatus text","uploadDate text"];
    createTable("tblheader",columnNames);
}

function createTableCounts() {
    var columnNames = ["countsUID text primary key","headerUID text","scheduleUID text","projectUID text","projectType text","counter1 text","counter2 text","counter3 text","countStatus text","machineID text","dateCreated text","dateModified text","gps text","uploadStatus text","uploadDate text"];
    createTable("tblcounts",columnNames);
}

function createTableParties() {
    var columnNames = ["partiesUID text primary key","headerUID text","scheduleUID text","projectUID text","partyNumber text","partySize text","partyComments text","partyStatus text","machineID text","dateCreated text","dateModified text","gps text","uploadStatus text","uploadDate text","verifiedStatus text","verifiedBy text","verifiedComments text","verifiedDateTime text"];
    createTable("tblparties",columnNames);
}

function createTableAnglers() {
    var columnNames = ["anglerUID text primary key","partiesUID text","headerUID text","scheduleUID text","projectUID text","anglerNumber text","timeStarted text","timeContactedStopped text","fishingHours text","zipCode text","destination text","satisfaction text","trip text","race text","fishType text","lure text","method text","anglerPreference text","loc text","boat text","optionalQ1 text","optionalQ2 text","optionalQ3 text","optionalQ4 text","optionalQ5 text","optionalQ6 text","anglerComments text","anglerStatus text","machineID text","dateCreated text","dateModified text","gps text","uploadStatus text","uploadDate text","verifiedStatus text","verifiedBy text","verifiedComments text","verifiedDateTime text"];
    createTable("tblanglers",columnNames);
}

function createTableFishCaught() {
    var columnNames = ["fishCaughtUID text primary key","anglerUID text","partiesUID text","headerUID text","scheduleUID text","projectUID text","speciesCode text","category text","numberCaught text","length text","measured text","photoTaken text","imageURL text","fishStatus text","machineID text","dateCreated text","dateModified text","gps text","uploadStatus text","uploadDate text"];
    createTable("tblfishcaught",columnNames);
}

function createTableProjectsValidate() {
    var columnNames = ["projectUID text PRIMARY KEY","tblHeader_secchi text","tblHeader_waterTemperature text","tblHeader_waterLevel text","tblAnglers_zipCode text","tblAnglers_destination text","tblAnglers_satisfaction text","tblAnglers_trip text","tblAnglers_race text","tblAnglers_fishType text","tblAnglers_lure text","tblAnglers_method text","tblAnglers_loc text","tblAnglers_boat text"];
    createTable("tblprojectsvalidate",columnNames);
}
