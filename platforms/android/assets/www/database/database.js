// Load at startup
// Opens database
var myDB = null;
document.addEventListener('deviceready', function() {
    myDB = window.sqlitePlugin.openDatabase({name: 'creelDb', location: 'default'});
    // dropAllTables();
});

function closeDB() {
    myDB.close(function () {
        console.log("DB closed!");
    }, function (error) {
        console.log("Error closing DB:" + error.message);
    });
}
