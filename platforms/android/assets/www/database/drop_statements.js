function dropTable(table) {
    // Column Name Value String
    var query = "";
    myDB.transaction(function(transaction) {
        var drop_query = 'DROP TABLE IF EXISTS ' + table;
        transaction.executeSql(drop_query, [],
            function(tx,result){
                //navAlert("Dropped");
            },
            function(error) {
                //navAlert("Dropping Error: " + drop_query);
            }
        );
    });
}

function dropAllTables(){
    var tableNames = ["tblprojects","tblschedule","tblcounttimes","tblprojectspecies","tblheader","tblcounts","tblparties","tblanglers","tblfishcaught","tblspecies","tblprojectsvalidate"];
    for(var i=0; i<tableNames.length; i++){
        dropTable(tableNames[i]);
    }
}
