function insertIntoTable(table, columns, data) {
    // Column Name Query String
    var columnNamesQuery = "(";
    for (var i=0; i<columns.length; i++){
        if(i==0){
            columnNamesQuery = columnNamesQuery + columns[i];
        }else{
            columnNamesQuery = columnNamesQuery + ", " + columns[i];
        }
    }
    columnNamesQuery = columnNamesQuery + ")";

    // Column Name Value String
    var query = "";
    myDB.transaction(function(transaction) {
        var primary_id = "";
        for(var i=0; i<data.length; i++){
            var row = data[i];
            // Create insert query
            var columnValuesArray = [];
            var columnValuesQuery = "(";
            for(var j=0; j<columns.length; j++){
                var val = row[columns[j]];
                if(j==0){
                    primary_id = val;
                }
                if(val === undefined || val == null){
                    val = "";
                }
                columnValuesArray[j] = val;
                if(j<columns.length-1){
                    columnValuesQuery = columnValuesQuery + "?,";
                } else if(j==columns.length-1){
                    columnValuesQuery = columnValuesQuery + "?)";
                }
            }

            // Insert query
            var project_query = 'INSERT OR REPLACE INTO ' + table + columnNamesQuery + ' VALUES ' + columnValuesQuery + ";";
            transaction.executeSql(project_query, columnValuesArray,
                function(tx,result){
                    if(table == "tblparties"){
                        //alert("Query: " + project_query);
                        //navAlert("Data inserted/replaced in table: " + table);
                    }
                },
                function(error) {
                    navAlert("Error: " + return_val + " " + project_query);
                }
            );
        }
    });
}

// Inserted in tablet on download from server
function insertTblProjects(dataArray){
	var columns = ["projectUID","projectName","projectType","projectLocation","projectDescription","projectStartDate","projectStopDate","optionalQ1","optionalQ2","optionalQ3","optionalQ4","optionalQ5","optionalQ6"];
	insertIntoTable("tblprojects",columns, dataArray);
}

function insertTblSchedule(dataArray){
	var columns = ["scheduleUID","projectUID","surveyStartDateTime","surveyStopDateTime","surveyLocation","surveyStatus"];
	insertIntoTable("tblschedule",columns, dataArray);
}

function insertTblCountTimes(dataArray){
	var columns = ["countTimesUID","scheduleUID","projectUID","countStartDateTime","countStopDateTime"];
	insertIntoTable("tblcounttimes",columns, dataArray);
}

function insertTblSpecies(dataArray){
	var columns = ["speciesCode","speciesCommonName","speciesLatinName"];
	insertIntoTable("tblspecies",columns, dataArray);
}

function insertTblProjectSpecies(dataArray){
	var columns = ["projectUID","speciesCode","speciesNameFirst","speciesNameLast","inList","inLargeList","inAnglerPref","warnMinSize","warnMaxSize"];
	insertIntoTable("tblprojectspecies",columns, dataArray);
}

function insertTblProjectsValidate(dataArray){
    var columns = ["projectUID","tblHeader_secchi","tblHeader_waterTemperature","tblHeader_waterLevel","tblAnglers_zipCode","tblAnglers_destination","tblAnglers_satisfaction","tblAnglers_trip","tblAnglers_race","tblAnglers_fishType","tblAnglers_lure","tblAnglers_method","tblAnglers_loc","tblAnglers_boat"];
    insertIntoTable("tblprojectsvalidate",columns,dataArray);
}

// CREATE OBJECT TO INSERT IN TABLET DB
function createObjectArray(keys,values){
    var arr = [];
    var obj = {};
    for(var i=0; i<keys.length; i++){
        obj[keys[i]] = values[i];
    }
    arr.push(obj);
    return arr;
}


// Inserted from tablet to server
function insertTblHeader(dataArray){
	var columns = ["headerUID","scheduleUID","projectUID","clerk","secchi","waterTemperature","waterLevel","clerkComments","headerStatus","machineID","dateCreated","dateModified","gps","uploadStatus","uploadDate"];
	var arr = createObjectArray(columns, dataArray);
	insertIntoTable("tblheader",columns, arr);
}

function insertTblCounts(dataArray){
	var columns = ["countsUID","headerUID","scheduleUID","projectUID","projectType","counter1","counter2","counter3","countStatus","machineID","dateCreated","dateModified","gps","uploadStatus","uploadDate"];
	var arr = createObjectArray(columns, dataArray);
	insertIntoTable("tblcounts",columns, arr);
}

function insertTblParties(dataArray){
	var columns = ["partiesUID","headerUID","scheduleUID","projectUID","partyNumber","partySize","partyComments","partyStatus","machineID","dateCreated","dateModified","gps","uploadStatus","uploadDate","verifiedStatus","verifiedBy","verifiedComments","verifiedDateTime"];
	var arr = createObjectArray(columns, dataArray);
	insertIntoTable("tblparties",columns,arr);
}

function insertTblAnglers(dataArray){
	var columns = ["anglerUID","partiesUID","headerUID","scheduleUID","projectUID","anglerNumber","timeStarted","timeContactedStopped","fishingHours","zipCode","destination","satisfaction","trip","race","fishType","lure","method","anglerPreference","loc","boat","optionalQ1","optionalQ2","optionalQ3","optionalQ4","optionalQ5","optionalQ6","anglerComments","anglerStatus","machineID","dateCreated","dateModified","gps","uploadStatus","uploadDate","verifiedStatus","verifiedBy","verifiedComments","verifiedDateTime"];
	var arr = createObjectArray(columns, dataArray);
	insertIntoTable("tblanglers",columns, arr);
}

function insertTblFishCaught(dataArray){
	var columns = ["fishCaughtUID","anglerUID","partiesUID","headerUID","scheduleUID","projectUID","speciesCode","category","numberCaught","length","measured","photoTaken","imageURL","fishStatus","machineID","dateCreated","dateModified","gps","uploadStatus","uploadDate"];
	var arr = createObjectArray(columns, dataArray);
	insertIntoTable("tblfishcaught",columns, arr);
}
