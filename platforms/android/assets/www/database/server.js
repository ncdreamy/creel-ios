// SERVER URLs
var serverURL = 'http://ec2-54-205-153-27.compute-1.amazonaws.com/server/selectQuery.php';
var uploadURL = 'http://ec2-54-205-153-27.compute-1.amazonaws.com/server/upload.php';
var uploadImageURL = encodeURI('http://ec2-54-205-153-27.compute-1.amazonaws.com/server/uploadImages.php');

// Downloads all data from server at initial app loading or whenever called
var tblInsertList = ['insertTblProjects','insertTblSchedule','insertTblCountTimes','insertTblProjectSpecies','insertTblSpecies','insertTblProjectsValidate'];
var tableNames = ['tblprojects','tblschedule','tblcounttimes','tblprojectspecies','tblspecies','tblprojectsvalidate'];
var completed = false;
function downloadAllData(count, page){
    if(count < tableNames.length){
        var data = {table: tableNames[count]};
        var functionName = tblInsertList[count];

        // Get Data from database
        $.ajax({
            url:serverURL,
            type:'POST',
            data:data,
            dataType:'json',
            success:function(result){
                // Creates Function to run from the string array
                var func = window[functionName];
                if(typeof func == "function"){
                    func(result); // passes the json array as parameter
                }
                if(count == tableNames.length-1){
                    completed = true;
                    closeSpinner(completed, page);
                }
                downloadAllData(count + 1, page);
            },
            error:function(xhr, status, error){
                //navAlert("Table " + count);
                //navAlert('Error:' + error);
                //navAlert('xhr:' + JSON.stringify(xhr));
                //navAlert('status:' + status);
            }
        });
    }
}

// Close Spinner and display alert
function closeSpinner(completed, page){
    if(completed){
        $("#spinnerBackground").fadeOut('fast');
        if(page == "index"){ // Homepage
            navAlert('Projects have been downloaded')
            getProjectList();
        } else { // Utilities
            navAlert("Projects have been updated");
        }
    } else{ // If all data was not downloaded
        navAlert("Unable to download projects");
    }
}

/************ UPLOAD DATA **************/
// Sends tables to server's PHP file
function uploadData(uploadStatus){
    var selectFunctionArray = ['selectTableHeader','selectTableCounts','selectTableParties','selectTableAnglers','selectTableFishCaught'];
	var tableNameArray = ['tblheader','tblcounts','tblparties','tblanglers','tblfishcaught'];
	var allTables = [];
	selectTables(tableNameArray, uploadStatus, allTables);
}

// GET DATABASE TABLES
var count = 0;
var allTables = [];
function selectTables(tableNameArray, uploadStatus, allTables) {
    var where = "";
    if(uploadStatus != null){
        where = where + " WHERE uploadStatus = '" + uploadStatus + "'";
    }

    // Get Table Name
    var tableName = tableNameArray[count];
    var tableData = {};

    // Select rows from table with respective uploadStatus
    myDB.transaction(function(transaction) {
		// Create query
		var query= "SELECT * FROM " + tableName + where;
		// Ignore imageURL from tblfishcaught
		// Images will be uploaded separately
		// FOR NOW
		if(tableName == "tblfishcaught"){
		    query = "SELECT fishCaughtUID, anglerUID, partiesUID, headerUID, scheduleUID, projectUID, speciesCode, category, numberCaught, length, measured, photoTaken, fishStatus, machineID, dateCreated, dateModified, gps, uploadStatus, uploadDate FROM tblfishcaught" + where;
		}
        // Execute query
        transaction.executeSql(query, [],
        function(tx, result) {
            // If non-uploaded records found
            // Upload only those records
            if(result.rows.length > 0){
                // If data exists for corresponding table
                // Create an object for table
                // Add object to allTables array
                var rows = [];
                for(var i=0; i<result.rows.length; i++){
                    var row = {};
                    rows.push(result.rows.item(i));
                }
                tableData[tableName] = rows;
                allTables.push(tableData);
            }
            if(count < tableNameArray.length-1){
                count++;
                selectTables(tableNameArray, uploadStatus, allTables);
            } else {
                count = 0;
                onComplete(allTables, uploadStatus);
            }
        },
        function(error) {
            //navAlert("Error occurred while selecting table:" + JSON.stringify(error));
        });
    });
}

// Upload to server
function onComplete(uploadData, uploadStatus){
    var exist = 0;
    for(var i=0; i<uploadData.length; i++){
        var table = uploadData[i];
        $.each(table,function(key,value){
            if(value.length > 0){
                exist++;
            }
        });
    }
    if(uploadStatus == 0){
        if(exist > 0){
            $.ajax({
                url:uploadURL,
                type:'POST',
                data:{uploadData:uploadData},
                dataType:'text',
                success:function(result){},
                error:function(xhr, status, error){
                    navAlert("Unable to upload data! Check your internet connection and try again");
                },
                complete:function(jqXHR, code){
                    if(jqXHR.status == 201){
                        navAlert("Data uploaded successfully");
                        // Update all records' status to 1 and insert uploadDate
                        updateRecords(uploadData);
                    } else if (jqXHR.status == 200){
                        navAlert("ERROR: Unable to upload data");
                    }
                }
            });
        } else{
            navigator.notification.confirm(
                'All existing data has been uploaded. Do you wish to double check?',    // message
                function(buttonIndex){
                    if(buttonIndex == 1){
                        uploadAllData(1);
                    }
                },
                'Confirm',
                ['OK','Cancel']
            );
        }
    } else{
        // uploadStatus == 1
        // Upload data only if data exists
        if(exist > 0){
            $.ajax({
                url:uploadURL,
                type:'POST',
                data:{uploadData:uploadData},
                dataType:'text',
                success:function(result){},
                error:function(xhr, status, error){
                    navAlert("Unable to upload data! Check your internet connection and try again");
                },
                complete:function(jqXHR, code){
                    if(jqXHR.status == 201){
                        navAlert("Double checked! All data has been uploaded");
                        // Update all records' status to 1 and insert uploadDate
                        updateRecords(uploadData);
                    } else if (jqXHR.status == 200){
                        navAlert("ERROR: Unable to upload data");
                    }
                }
            });
        } else {
            navAlert("No records to upload");
        }
    }
}

// Update all tables when successfully uploaded
function updateRecords(uploadData){
    var uploadDate = dateFormat(new Date(),"yyyy-mm-dd hh:MM:ss");
    var count = 0;
    for(var i=0; i<uploadData.length; i++){
        var table = uploadData[i];
        $.each(table,function(key,value){
            var tableName = key;
            myDB.transaction(function(transaction){
                var query = 'UPDATE '+tableName+' SET uploadStatus = "1", uploadDate = ? WHERE uploadStatus = "0";';
                transaction.executeSql(query, [uploadDate],
                    function(tx, result) {
                        count = count+1;
                        if(count == uploadData.length-1){
                            //navAlert("Uploaded and updated all records");
                        }
                    },
                    function(err){
                        navAlert("Update Error: " + JSON.stringify(err));
                    }
                );
            });
        });
    }
}
/************ END UPLOAD DATA **************/

/************ UPLOAD IMAGES **************/
// UPLOAD FISH CAUGHT IMAGES
var errorUploading = 0;
function uploadFishCaughtImages() {
    myDB.transaction(function(transaction) {
        // Create query
        var query= "SELECT fishCaughtUID, imageURL FROM tblfishcaught WHERE imageURL != ''";
        // Execute query
        transaction.executeSql(query, [],
            function(tx, result) {
                if(result.rows.length > 0){
                    var options = new FileUploadOptions();
                    options.fileKey = 'file';
                    options.mimeType = "image/jpeg"; //image type
                    options.httpMethod = "POST";
                    var ft = new FileTransfer();
                    var row = {};
                    var count = 0;
                    errorUploading = 0;
                    for(var i=0; i<result.rows.length; i++){
                        row =result.rows.item(i);
                        var imageURL = row["imageURL"];
                        var fishCaughtUID = row["fishCaughtUID"];
                        options.fileName = imageURL.substr(imageURL.lastIndexOf('/') + 1);
                        options.params = {
                            fishCaughtUID: fishCaughtUID
                        };
                        options.chunkedMode = false;
                        options.header = {
                            Connection:"close"
                        };
                        ft.upload(imageURL, uploadImageURL, function(msg){
                            onUploadSuccess(msg, count, result.rows.length, imageURL, fishCaughtUID);
                            count = count + 1;
                        }, onUploadError, options, true);
                        //ft.abort(); // Aborts progress mid-upload
                    }
                } else {
                    navAlert("No images to upload!");
                }
            },
            function(error){
                navAlert("Error in "); // error.source
            }
        );
    });
}

function onUploadSuccess(msg, counter, total_files, imageURL, fishCaughtUID){
    if(msg.responseCode == 200){
        errorUploading = errorUploading + 1;
    } else{
        // Remove URL from tablet DB
        myDB.transaction(function(transaction){
            var query = "UPDATE tblfishcaught SET imageURL = '' WHERE fishCaughtUID = ?";
            transaction.executeSql(query,[fishCaughtUID],
                function(tx, result){
                    if(counter == total_files - 1){
                        // Updated
                    }
                },
                function(err){
                    // Error
                }
            );

        });
        // Delete Image
        window.resolveLocalFileSystemURL(imageURL, function(result){
            // Remove the URL from database
            result.remove(function(){
                // navAlert("Image Removed");
            });
        });
    }

    if(counter == (total_files - 1)){
        if(errorUploading > 0){
            navAlert("All non-uploaded images uploaded");
        } else {
            navAlert("Successfully Uploaded");
        }
    }
}

function onUploadError(error){
    navAlert("Failed to upload image to server: " + error.source);
}
