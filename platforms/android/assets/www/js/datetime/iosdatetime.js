/** RETURNS CURRENT DATE TIME in yyyy/mm/dd hh:mm:ss FORMAT **/
function getCurrentDateTime(){
    var now = new Date();
    //now = timeZoneOffset(now);
    
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    if(month < 10){
        month = addZeroPrefix(month);
    }
    var day = now.getDate();
    if(day < 10){
        day = addZeroPrefix(day);
    }
    var hour = now.getHours();
    if(hour < 10){
        hour = addZeroPrefix(hour);
    }
    var minutes = now.getMinutes();
    if(minutes < 10){
        minutes = addZeroPrefix(minutes);
    }
    var seconds = now.getSeconds();
    if(seconds < 10){
        seconds = addZeroPrefix(seconds);
    }
    var formattedDateTime = year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
    return formattedDateTime;
}

/** RETURNS CURRENT DATE TIME in hh:mm FORMAT **/
function getCurrentTime(){
    var now = new Date();
    //now = timeZoneOffset(now);
    
    var hour = now.getHours();
    if(hour < 10){
        hour = addZeroPrefix(hour);
    }
    var minutes = now.getMinutes();
    if(minutes < 10){
        minutes = addZeroPrefix(minutes);
    }
    var formattedDateTime = hour + ":" + minutes;
    return formattedDateTime;
}

/** RETURNS FULL DATE AND TIME **/
function getFullDateTime(value){
    if(value != null){
        newDate = new Date(value);
    }
    
    var year = newDate.getFullYear();
    var month = newDate.getMonth() + 1;
    if(month < 10){
        month = addZeroPrefix(month);
    }
    var day = newDate.getDate();
    if(day < 10){
        day = addZeroPrefix(day);
    }
    var hour = newDate.getHours();
    if(hour < 10){
        hour = addZeroPrefix(hour);
    }
    var minutes = newDate.getMinutes();
    if(minutes < 10){
        minutes = addZeroPrefix(minutes);
    }
    var seconds = newDate.getSeconds();
    if(seconds < 10){
        seconds = addZeroPrefix(seconds);
    }
    var formattedDateTime = year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
    return formattedDateTime;
}

/** RETURNS FORMATTED DATE in mm/dd/yyyy **/
function getFormattedDate(value){
    var now = new Date(value);
    //now = timeZoneOffset(now);
    
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    if(month < 10){
        month = addZeroPrefix(month);
    }
    var day = now.getDate();
    if(day < 10){
        day = addZeroPrefix(day);
    }
    var formattedDate = month + "/" + day + "/" + year;
    return formattedDate;
}

/** RETURNS FORMATTED DATE in DATABASE FORMAT yyyy/mm/dd **/
function getDatabaseFormatDate(value){
    var now = new Date(value);
    //now = timeZoneOffset(now);
    
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    if(month < 10){
        month = addZeroPrefix(month);
    }
    var day = now.getDate();
    if(day < 10){
        day = addZeroPrefix(day);
    }
    var formattedDate = year + "-" + month + "-" + day;
    return formattedDate;
}

/** RETURNS FORMATTED TIME in hh:mm **/
function getFormattedTime(){
    var now = new Date();
    //now = timeZoneOffset(now);
    
    var hour = now.getHours();
    if(hour < 10){
        hour = addZeroPrefix(hour);
    }
    var minutes = now.getMinutes();
    if(minutes < 10){
        minutes = addZeroPrefix(minutes);
    }
    var formattedTime = hour + ":" + minutes;
    return formattedTime;
}


/** RETURNS -1 if date1 is before date 2; 0 if equal; else RETURNS 1
    INPUT expected in String format
 **/
function iosTimeDiff(date1, date2){
    var date1 = date1.getTime();
    var date2 = date2.getTime();
    if(date1 < date2){
        return -1;
    } else if(date1 == date2){
        return 0;
    } else{
        return 1;
    }
}

// Format date to AM PM
function formatAMPM(date) {
    var hours = date.getHours();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12; // mod
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var strTime = hours + ' ' + ampm; // ':' + minutes +
    return strTime;
}

// Adds 0 to any single digit day, month, etc.
function addZeroPrefix(value){
    var newVal = "0" + value.toString();
    return newVal;
}

// Adds T between date and time for IOS dates
function addT(value){
    return value.replace(" ","T");
}

// ADD 5 MINUTES TO SURVEY TIMER
function addMinutes(date, minutes) {
    var newDate = new Date(date.getTime() + minutes*60000);
    return newDate;
}

// CALCULATE DATE DIFFERENCE
function dateDiff(firstDate, secondDate){
    var oneDay = 24*60*60*1000;
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    return diffDays;
}

// Bypass Timezone offset
function timeZoneOffset(date){
    var date = new Date(date);
    var offset = date.getTimezoneOffset()*60*1000;
    var finalDate = date.getTime() + offset;
    return new Date(finalDate);
}
