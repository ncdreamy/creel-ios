/* Function to delete all local storage items */
function removeAllLocalStorageItems(){
    window.localStorage.removeItem("secchi");
    window.localStorage.removeItem("wLevel");
    window.localStorage.removeItem("wTemp");
    window.localStorage.removeItem("partyNumber");
    window.localStorage.removeItem("partySize");
    window.localStorage.removeItem("anglerNumber");
    window.localStorage.removeItem("tab1");
    window.localStorage.removeItem("tab2");
    window.localStorage.removeItem("tab3");
    window.localStorage.removeItem("partyComments");
    window.localStorage.removeItem("surveyComments");
}
