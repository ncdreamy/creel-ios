// Load at startup
// Opens database
var myDB = null;
document.addEventListener("deviceready", openDatabase, false);

function openDatabase(){
    myDB = window.sqlitePlugin.openDatabase({name: "creelDb", location: "default"});
    // alert("db: " + JSON.stringify(myDB));
    // dropAllTables();
}

function closeDB() {
    myDB.close(function () {
        console.log("DB closed!");
    }, function (error) {
        console.log("Error closing DB:" + error.message);
    });
}
