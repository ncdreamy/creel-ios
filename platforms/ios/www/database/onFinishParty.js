// Callback function
function onFinishParty(){
    var practice = window.localStorage.getItem("practice");
    if(practice == null){
        $("#spinnerBackground").show();

        // GET JSON DATA
        // UIDs and other important values
        var projectUID = window.localStorage.getItem("projectUID");
        var scheduleUID = window.localStorage.getItem("scheduleUID");
        var partyCount = window.localStorage.getItem("numberOfParties");
        partyCount = parseInt(partyCount) + 1; // Update counter value
        window.localStorage.setItem("partyCount",partyCount);
        var machineID = device.uuid;
        var gps = "";
        var uploadStatus = "0";

        // DATETIME
        var dateCreated = "";
        if(device.platform.toLowerCase() == "android"){
            dateCreated = dateFormat(new Date(),"yyyy-mm-dd HH:MM:ss");
        } else if(device.platform.toLowerCase() == "ios"){
            dateCreated = getCurrentDateTime();
        }
        var uploadDate = "";
        var dateModified = dateCreated;
        
        // Verification
        var verifiedStatus = "0";
        var verifiedBy = "";
        var verifiedComments = "";
        var verifiedDateTime = "0000-00-00 00:00:00";

        // SAVE ALL DATA TO DATABASE
        var headerUID = window.localStorage.getItem("headerUID");

        /***** tblParties *****/
        var partiesUID = window.localStorage.getItem("partiesUID");
        var partySize = window.localStorage.getItem("partySize");
        var partyStatus = "0"; // NOT REFUSED IF COMING FROM THE PARTY COMMENTS PAGE
        var partyComments = $("#comments").val().trim();

        var partyArray = [partiesUID,headerUID,scheduleUID,projectUID,partyCount.toString(),partySize,partyComments,partyStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate,verifiedStatus,verifiedBy,verifiedComments,verifiedDateTime];
        insertTblParties(partyArray);

        var anglerView = window.localStorage.getItem("anglerView");
        if(anglerView == "tabular"){
            /***** tblAnglers *****/
            var tab1 = JSON.parse(window.localStorage.getItem("tab1")).reverse();
            var tab2 = JSON.parse(window.localStorage.getItem("tab2"));
            var tab3 = JSON.parse(window.localStorage.getItem("tab3"));

            // GET ALL TABS' DATA AND UPLOAD TO TABLET DATABASE
            for(var i=0; i<tab1.length; i++){
                var newAnglerUID = randomNumber();
                var tab1_list = tab1[i];
                var anglerNumber = tab1_list["anglerNumber"];
                var refused = tab1_list["refused"];
                var anglerStatus = "0";
                if(refused){
                    anglerStatus = "1";
                }

                // SET FOLLOWING VALUES
                var timeStarted = "";
                var timeContactedStopped = "";
                var fishingHours = "";
                var trip = "";
                var zipCode = "";
                var destination = "";
                var satisfaction = "";
                var race = "";
                var fishType = "";
                var lure = "";
                var method = "";
                var anglerPreference = "";

                // Optional Questions
                var optionalQ1 = "";
                var optionalQ2 = "";
                var optionalQ3 = "";
                var optionalQ4 = "";
                var optionalQ5 = "";
                var optionalQ6 = "";

                // FIELDS DO NOT EXIST FOR THE FOLLOWING IN APP
                // WILL NEED TO BE ADDED BEFORE RELEASE
                var anglerComments = "";
                // for stream creel
                var boat = "";
                var loc = "";
                var anglerArray = [];
                if(refused){
                    anglerArray = [newAnglerUID,partiesUID,headerUID,scheduleUID,projectUID,anglerNumber,timeStarted,timeContactedStopped,fishingHours,zipCode,destination,satisfaction,trip,race,fishType,lure,method,anglerPreference,loc,boat,optionalQ1,optionalQ2,optionalQ3,optionalQ4,optionalQ5,optionalQ6,anglerComments,anglerStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate,verifiedStatus,verifiedBy,verifiedComments,verifiedDateTime];
                    insertTblAnglers(anglerArray);
                }else {
                    // SET ANY EXISTING VALUES
                    // TAB 1
                    timeStarted = tab1_list["tStart"];
                    timeContactedStopped = tab1_list["tStop"];
                    fishingHours = tab1_list["fHours"];
                    trip = tab1_list["completion"];
                    zipCode = tab1_list["zip"];
                    destination = tab1_list["destination"];

                    // TAB 2
                    if(tab2 != null){
                        tab2 = tab2.reverse();
                        for(var j=0; j<tab2.length; j++){
                            if(tab2[j]["anglerNumber"] == anglerNumber){
                                if(tab2[j]["satisfaction"] != null){
                                    satisfaction = tab2[j]["satisfaction"];
                                }
                                if(tab2[j]["race"] != null){
                                    race = tab2[j]["race"];
                                }
                                if(tab2[j]["fType"] != null){
                                    fishType = tab2[j]["fType"];
                                }
                                if(tab2[j]["lure"] != null){
                                    lure = tab2[j]["lure"];
                                }
                                if(tab2[j]["method"] != null){
                                    method = tab2[j]["method"];
                                }
                                if(tab2[j]["prefSpec"] != null){
                                    anglerPreference = tab2[j]["prefSpec"];
                                }
                            }
                        }
                    }

                    // TAB 3
                    var tab3_list = tab3;
                    if(tab3_list != null){
                        tab3 = tab3.reverse();
                        for(var j=0; j<tab3_list.length; j++){
                            if(tab3_list[j]["anglerNumber"] == anglerNumber){
                                if(tab3_list[j]["optionalQ1"] != null){
                                    optionalQ1 = tab3_list[j]["optionalQ1"];
                                }
                                if(tab3_list[j]["optionalQ2"] != null){
                                    optionalQ2 = tab3_list[j]["optionalQ2"];
                                }
                                if(tab3_list[j]["optionalQ3"] != null){
                                    optionalQ3 = tab3_list[j]["optionalQ3"];
                                }
                                if(tab3_list[j]["optionalQ4"] != null){
                                    optionalQ4 = tab3_list[j]["optionalQ4"];
                                }
                                if(tab3_list[j]["optionalQ5"] != null){
                                    optionalQ5 = tab3_list[j]["optionalQ5"];
                                }
                                if(tab3_list[j]["optionalQ6"] != null){
                                    optionalQ6 = tab3_list[j]["optionalQ6"];
                                }
                            }
                        }
                    }

                    anglerArray = [newAnglerUID,partiesUID,headerUID,scheduleUID,projectUID,anglerNumber,timeStarted,timeContactedStopped,fishingHours,zipCode,destination,satisfaction,trip,race,fishType,lure,method,anglerPreference,loc,boat,optionalQ1,optionalQ2,optionalQ3,optionalQ4,optionalQ5,optionalQ6,anglerComments,anglerStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate];
                    insertTblAnglers(anglerArray);

                    /***** tblFishCaught *****/
                    var fishCaught = JSON.parse(window.localStorage.getItem("fishCaught"));
                    if(fishCaught != null){
                        fishCaught = fishCaught.reverse();
                        for(var j=0; j<fishCaught.length; j++){
                            if(fishCaught[j]["anglerNumber"] == anglerNumber){
                                var fishList = fishCaught[j]["fish"];
                                for (var k=0; k<fishList.length; k++){
                                    var fish = fishList[k];
                                    var fishCaughtUID = randomNumber();
                                    var species = "";
                                    var category = "";
                                    var fishCount = "";
                                    var length = "";
                                    var measured = "";
                                    var imageURL = "";
                                    var fishStatus = "0";
                                    var photoTaken = "0";

                                    if(fish["species"] != null){
                                        species = fish["species"];
                                    }
                                    if(fish["category"] != null){
                                        category = fish["category"];
                                    }
                                    if(fish["fishCount"] != null){
                                        fishCount = fish["fishCount"];
                                    }
                                    if(fish["length"] != null){
                                        length = fish["length"];
                                    }
                                    if(fish["measured"] != null){
                                        measured = fish["measured"];
                                    }
                                    if(fish["image"] != null){
                                        imageURL = fish["image"];
                                        photoTaken = "1";
                                    }

                                    var fishCaughtArray = [fishCaughtUID,newAnglerUID,partiesUID,headerUID,scheduleUID,projectUID,species,category,fishCount,length,measured,photoTaken,imageURL,fishStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate];
                                    insertTblFishCaught(fishCaughtArray);
                                }
                            }
                        }
                    }
                } // end if
            } // end for loop
        } // end if anglerview == 'tabular'
        /*********** ANGLER VIEW ************/
        else if(anglerView == "angler"){
            var anglerList = JSON.parse(window.localStorage.getItem("anglerList"));
            var anglerUID;
            var angler;
            var refused;
            for(var i=0; i<anglerList.length; i++){
                angler = anglerList[i];
                anglerUID = randomNumber();
                anglerNumber = angler["anglerNumber"];
                refused = angler["refused"];
                var anglerStatus = "0";
                if(refused){
                    anglerStatus = "1";
                }

                // SET FOLLOWING VALUES
                var timeStarted = "";
                var timeContactedStopped = "";
                var fishingHours = "";
                var trip = "";
                var zipCode = "";
                var destination = "";
                var satisfaction = "";
                var race = "";
                var fishType = "";
                var lure = "";
                var method = "";
                var anglerPreference = "";

                // Optional Questions
                var optionalQ1 = "";
                var optionalQ2 = "";
                var optionalQ3 = "";
                var optionalQ4 = "";
                var optionalQ5 = "";
                var optionalQ6 = "";

                // FIELDS DO NOT EXIST FOR THE FOLLOWING IN APP
                // WILL NEED TO BE ADDED BEFORE RELEASE
                var anglerComments = "";
                // for stream creel
                var boat = "";
                var loc = "";
                var anglerArray = [];
                var fishCaught = [];
                if(refused){
                    anglerArray = [anglerUID,partiesUID,headerUID,scheduleUID,projectUID,anglerNumber,timeStarted,timeContactedStopped,fishingHours,zipCode,destination,satisfaction,trip,race,fishType,lure,method,anglerPreference,loc,boat,optionalQ1,optionalQ2,optionalQ3,optionalQ4,optionalQ5,optionalQ6,anglerComments,anglerStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate,verifiedStatus,verifiedBy,verifiedComments,verifiedDateTime];
                    insertTblAnglers(anglerArray);
                }else {
                    // SET ANY EXISTING VALUES
                    // TAB 1
                    timeStarted = angler["timeStarted"];
                    timeContactedStopped = angler["timeStopped"];
                    fishingHours = angler["fHours"];
                    trip = angler["completion"];
                    zipCode = angler["zip"];
                    destination = angler["location"];
                    satisfaction = angler["satisfaction"];
                    race = angler["race"];;
                    fishType = angler["fType"];
                    lure = angler["lure"];
                    method = angler["method"];
                    anglerPreference = angler["prefSpec"];
                    
                    // OPTIONAL QUESTIONS
                    if(angler["optionalQ1"] != null){
                        optionalQ1 = angler["optionalQ1"];
                    }
                    if(angler["optionalQ2"] != null){
                        optionalQ2 = angler["optionalQ2"];
                    }
                    if(angler["optionalQ3"] != null){
                        optionalQ3 = angler["optionalQ3"];
                    }
                    if(angler["optionalQ4"] != null){
                        optionalQ4 = angler["optionalQ4"];
                    }
                    if(angler["optionalQ5"] != null){
                        optionalQ5 = angler["optionalQ5"];
                    }
                    if(angler["optionalQ6"] != null){
                        optionalQ6 = angler["optionalQ6"]
                    }

                    // TAB 2
                    fishCaught = angler["fishCaught"];
                    if(fishCaught != null){
                        var species = "";
                        var category = "";
                        var fishCount = "";
                        var length = "";
                        var measured = "";
                        var imageURL = "";
                        var fishStatus = "0";
                        var photoTaken = "0";
                        
                        // Search all fish
                        // alert("Fishcaught: " + fishCaught.length);
                        var fishCaughtArray;
                        var fish;
                        var fishcaughtUID;
                        for(var j=0; j<fishCaught.length; j++){
                            fish = fishCaught[j];
                            fishCaughtUID = randomNumber();
                            if(fish["species"] != null){
                                species = fish["species"];
                            }
                            if(fish["category"] != null){
                                category = fish["category"];
                            }
                            if(fish["fishCount"] != null){
                                fishCount = fish["fishCount"];
                            }
                            if(fish["length"] != null){
                                length = fish["length"];
                            }
                            if(fish["measured"] != null){
                                measured = fish["measured"];
                            }
                            if(fish["image"] != null){
                                imageURL = fish["image"];
                                photoTaken = "1";
                            }
    
                            fishCaughtArray = [fishCaughtUID,anglerUID,partiesUID,headerUID,scheduleUID,projectUID,species,category,fishCount,length,measured,photoTaken,imageURL,fishStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate];
                            insertTblFishCaught(fishCaughtArray);
                        } // Loop all fish
                    } // If fishcaught != null
                    
                    anglerArray = [anglerUID,partiesUID,headerUID,scheduleUID,projectUID,anglerNumber,timeStarted,timeContactedStopped,fishingHours,zipCode,destination,satisfaction,trip,race,fishType,lure,method,anglerPreference,loc,boat,optionalQ1,optionalQ2,optionalQ3,optionalQ4,optionalQ5,optionalQ6,anglerComments,anglerStatus,machineID,dateCreated,dateModified,gps,uploadStatus,uploadDate,verifiedStatus,verifiedBy,verifiedComments,verifiedDateTime];
                    insertTblAnglers(anglerArray);
                } // end if
            } // end for loop
        } // 
    } // end if practice == null

    // REDIRECT TO NEW PARTY
    window.setTimeout(function(){
		// Update party count
		var numberOfParties = window.localStorage.getItem("numberOfParties");
		if(numberOfParties == null){
			numberOfParties = "0";
		}
		numberOfParties = parseInt(numberOfParties) + 1;
		window.localStorage.setItem("numberOfParties",numberOfParties);
		
        $('#spinnerBackground').fadeOut('fast');
        window.location = "newParty.html";
    },2000);
}
