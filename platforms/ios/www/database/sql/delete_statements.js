function deleteTable(table) {
    // Column Name Value String
    var query = "";
    myDB.transaction(function(transaction) {
        var query = 'DELETE FROM ' + table;
        transaction.executeSql(query, [],
            function(tx,result){
                //navAlert("Table Emptied");
            },
            function(error) {
                //navAlert("Deleting Error: " + query);
            }
        );
    });
}

function deleteAllTables(){
    var tableNames = ["tblprojects","tblschedule","tblcounttimes","tblprojectspecies","tblspecies"];
    for(var i=0; i<tableNames.length; i++){
        deleteTable(tableNames[i]);
    }
}
