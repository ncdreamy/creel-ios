/** COUNTER FUNCTIONS **/
var a = 0;
function increase(){
	var textBox = document.getElementById("count1");
	if(parseInt(textBox.value) > 0){
	    a = parseInt(textBox.value);
	}
	a++;
	textBox.value = a;
}

function decrease(){
	var textBox = document.getElementById("count1");
	if(parseInt(textBox.value) > 0){
		navigator.notification.confirm(
			'Are you sure?',  // message
			function(buttonIndex){
				if(buttonIndex == 1){
				    a = parseInt(textBox.value);
					a--;
					if(a <= 0){
						a = 0;
					}
					textBox.value = a;
				}
			},                      // callback
			'Confirm',              // title
			['OK','Cancel']         // buttonName
		);
	}
}

var b = 0;
function increase2(){
	var textBox = document.getElementById("count2");
	if(parseInt(textBox.value) > 0){
        b = parseInt(textBox.value);
    }
	b++;
	textBox.value = b;
}

function decrease2(){
	var textBox = document.getElementById("count2");

	if(parseInt(textBox.value) > 0){
		navigator.notification.confirm(
			'Are you sure?',  // message
			function(buttonIndex){
				if(buttonIndex == 1){
				    b = parseInt(textBox.value);
					b--;
					if(b <= 0){
						b = 0;
					}
					textBox.value = b;
				}
			},                          // callback
			'Confirm',                  // title
			['OK','Cancel']             // buttonName
		);
	}
}

var c = 0;
function increase3(){
	var textBox = document.getElementById("count3");
	if(parseInt(textBox.value) > 0){
        c = parseInt(textBox.value);
    }
	c++;
	textBox.value = c;
}

function decrease3(){
	var textBox = document.getElementById("count3");

	if(parseInt(textBox.value) > 0){
		navigator.notification.confirm(
			'Are you sure?',  // message
			function(buttonIndex){
				if(buttonIndex == 1){
				    c = parseInt(textBox.value);
					c--;
					if(c <= 0){
						c = 0;
					}
					textBox.value = c;
				}
			},                          // callback
			'Confirm',                  // title
			['OK','Cancel']             // buttonName
		);
	}
}

/** INSERT COUNTER VALUES ON PAGE LOAD **/
// Set counter values
function insertCounterValues(){
    var count1 = window.localStorage.getItem("count1");
    var count2 = window.localStorage.getItem("count2");
    var count3 = window.localStorage.getItem("count3");
    if(count1 == null || isNaN(parseInt(count1))){
        count1 = 0;
    }
    if(count2 == null || isNaN(parseInt(count2))){
        count2 = 0;
    }
    if(count3 == null || isNaN(parseInt(count3))){
        count3 = 0;
    }
    $("#count1").val(parseInt(count1));
    $("#count2").val(parseInt(count2));
    $("#count3").val(parseInt(count3));
}

/** COUNTER LABELS **/
function counterLabels(){
	var creel_type = "access"; // window.localStorage.getItem("projectType");
    
	var access_stream = ["Add Angler", "Add N. Angler", "Add N. Contact"];
	var roving = ["Add Boat","Add Bank","Add PI Boat"];
	var labels;
	var font_size;

	if (creel_type.toLowerCase() === "access" || creel_type.toLowerCase() === "stream"){
		labels = access_stream;
		font_size = "15px";
	} else {
		labels = roving;
		font_size = "20px";
	}

	for (var i=0; i<labels.length; i++){
		$("#btn-count"+(i+1)).html(labels[i]);
		$("#btn-count"+(i+1)).css('font-size',font_size);
	}
    $(".small-button").css('font-size',font_size);
    $(".small-button").text("SUB");
}

/** TIMERS **/
// ADD 5 MINUTES TO SURVEY TIMER
function addMinutes(date, minutes) {
    var newDate = new Date(date.getTime() + minutes*60000);
    return newDate;
}

// SURVEY TIMER
function endSurveyCounter(){
	var platform = device.platform.toLowerCase();
	var practice = window.localStorage.getItem("practice");
    
	if(practice == null){ // If survey type is not a practice
		var surveyStartDateTime = window.localStorage.getItem("surveyStartDateTime");
        var surveyStopDateTime = window.localStorage.getItem("surveyStopDateTime");
        
        // Add T between date and time if iOS
        if(platform == "ios"){
            surveyStartDateTime = surveyStartDateTime.replace(" ","T");
            surveyStopDateTime = surveyStopDateTime.replace(" ","T");
        }
        surveyStartDateTime = new Date(surveyStartDateTime);
        surveyStopDateTime = new Date(surveyStopDateTime);
        
        // Offset timezone if iOS platform
        if(platform == "ios"){
            surveyStartDateTime = timeZoneOffset(surveyStartDateTime);
            surveyStopDateTime = timeZoneOffset(surveyStopDateTime);
        }
		var startTime = surveyStartDateTime.getTime();
        var stopTime = surveyStopDateTime.getTime();
        
        // Get current time
		var today = new Date();
		var now = today.getTime();
        
		// If survey has not officially begun
		// Counter should count down till start time
		if(startTime > now){
			$("#countdown").countdown(surveyStartDateTime,function(event){
				$(this).text('Survey Begins in: ' + event.strftime('%M minute(s) %S second(s)'));
			});
		}
		
		// If survey has begun but not ended
		var fired = false;
		if(startTime < now && stopTime > now){
			$("#countdown").countdown(surveyStopDateTime,function(event){
				$(this).text('Ends in: ' + event.strftime('%M minute(s) %S second(s)'));
				if(event.elapsed && !fired){
					fired = true;
					navigator.notification.confirm(
						'The survey has ended! Do you wish to add 5 more minutes? ',  // message
						function(buttonIndex){
							if(buttonIndex == 1){
								surveyStopTime = new Date();
								surveyStopTime = addMinutes(surveyStopDateTime,5);
								fired = false;
								incrementCounter(surveyStopDateTime);
							} else if(buttonIndex == 2) {
								fired = true;
								navAlert('You will be directed to the party comments page');
								window.location = "projectComments.html";
							}
						},                                                              // callback
						'Confirm',                                                      // title
						['Yes','End Survey']                                            // buttonName
					); // end navigator
				} // end if
			}); // end countdown
		} // end if
	} // if practice == null
	else{ // PRACTICE MODE
		var fired = false;
        var surveyStopTime = window.localStorage.getItem("practiceSurveyStopTime");
        surveyStopTime = surveyStopTime.replace("T"," "); // removes T in iOS time
        surveyStopTime = getFullDateTime(surveyStopTime);
		$("#countdown").countdown(surveyStopTime,function(event){
			$(this).text('Ends in: ' + event.strftime('%M minute(s) %S second(s)'));
			if(event.elapsed && !fired){
				fired = true;
				navigator.notification.confirm(
					'The survey has ended! Do you wish to add 5 more minutes? ',  // message
					function(buttonIndex){
						if(buttonIndex == 1){
							surveyStopTime = new Date();
                            surveyStopTime = timeZoneOffset(surveyStopTime);
							surveyStopTime = addMinutes(surveyStopTime,5);
							fired = false;
							incrementCounter(surveyStopTime);
						} else if(buttonIndex == 2) {
							fired = true;
							navAlert('You will be directed to the party comments page');
							window.location = "projectComments.html";
						}
					},                                                              // callback
					'Confirm',                                                      // title
					['Yes','End Survey']                                            // buttonName
				); // end navigator
			} // end if
		}); // end countdown		
	}
}

// If timer runs out
function incrementCounter(surveyStopTime){
	var fired = false;
	$("#countdown").countdown(surveyStopTime,function(event){
		$(this).text('Ends in: ' + event.strftime('%M minute(s) %S second(s)'));
		if(event.elapsed && !fired){
			fired = true;
			navigator.notification.confirm(
				'The survey has ended! Do you wish to add 5 more minutes? ',  // message
				function(buttonIndex){
					if(buttonIndex == 1){
						surveyStopTime = new Date();
						surveyStopTime = addMinutes(surveyStopTime,5);
						fired = false;
						incrementCounter(surveyStopTime);
					} else if(buttonIndex == 2) {
						fired = true;
						navAlert('You will be directed to the party comments page');
						window.location = "projectComments.html";
					}
				},                                                              // callback
				'Confirm',                                                      // title
				['Yes','End Survey']                                            // buttonName
			); // end navigator
		} // end if
	});	
}

// Hide or show counter depending on the type of project
function showCounter(){
    var projectType = window.localStorage.getItem("projectType");
    var alerted = false;
    if(projectType == "Stream" || projectType == "Roving"){
        // Get countTimes from database
        myDB.transaction(function(transaction){
            var scheduleUID = window.localStorage.getItem("scheduleUID");
            var query = "SELECT * from tblcounttimes WHERE scheduleUID = ?";
            transaction.executeSql(query,[scheduleUID],
                function(tx, result){
                    var counter = false;
                    alert(result.rows.length);
                    if(result.rows.length>0){
                        var now = new Date();
                        var platform = device.platform.toLowerCase();
                        for(var i=0; i<result.rows.length; i++){
                            countStartDateTime = result.rows.item(i)["countStartDateTime"];
                            countStopDateTime = result.rows.item(i)["countStopDateTime"];
                            
                            // if IOS
                            if(platform == "ios"){
                                countStartDateTime = timeZoneOffset(addT(countStartDateTime));
                                countStopDateTime = timeZoneOffset(addT(countStopDateTime));
                                // alert("Platform: " + platform + " time: " + countStartDateTime);
                            }
                            
                            if(countStartDateTime <= now && countStopDateTime > now){
                                counter = true;
                                break;
                            }
                        }
                    } else {
                        alerted = false;
                    }
                    
                    // If there are counttimes then check for when to show counter
                    alert(counter);
                    if(counter && !alerted){
                        alerted = true;
                        navAlert("Start counting now!");
                        $(".counter-row").removeClass("hidden");
                    } else {
                        $(".counter-row").addClass("hidden");
                    }
                }
            );
        });
    } else if(projectType == "Access"){
        $(".counter-row").removeClass("hidden");
    }
}


