// SERVER URLs
var server = window.localStorage.getItem("serverURL");
if(server == null || server == ""){
    server = 'http://ec2-54-90-92-56.compute-1.amazonaws.com';
    window.localStorage.setItem("serverURL", server);
}
var serverURL = server + '/server/selectQuery.php';

// Downloads all data from server at initial app loading or whenever called
var tblInsertList = ['insertTblProjects','insertTblSchedule','insertTblCountTimes','insertTblProjectSpecies','insertTblSpecies','insertTblProjectsValidate'];
var tableNames = ['tblprojects','tblschedule','tblcounttimes','tblprojectspecies','tblspecies','tblprojectsvalidate'];
var completed = false;

// Download function
function downloadAllData(count, page){
    if(count < tableNames.length){
        var data = {table: tableNames[count]};
        var functionName = tblInsertList[count];

        // Get Data from database
        $.ajax({
            url:serverURL,
            type:'POST',
            data:data,
            dataType:'json',
            success:function(result){
                // Creates Function to run from the string array
                var func = window[functionName];
                if(typeof func == "function"){
                    func(result); // passes the json array as parameter
                }
                downloadAllData(count + 1, page);
               
                // End recursive loop
                if(count == tableNames.length-1){
                    completed = true;
                    closeSpinner(completed, page);
                }
            },
            error:function(xhr, status, error){
                navAlert("Table " + count);
                navAlert('Error:' + error);
                navAlert('xhr:' + JSON.stringify(xhr));
                navAlert('status:' + status);
            }
        });
    }
}

// Close Spinner and display alert
function closeSpinner(completed, page){
    if(completed){
        $("#spinnerBackground").fadeOut('fast');
        if(page == "index"){ // Homepage
            navAlert('Projects have been downloaded')
            getProjectList();
        } else { // Utilities
            navAlert("Projects have been updated");
        }
    } else{ // If all data was not downloaded
        navAlert("Unable to download projects");
    }
}
