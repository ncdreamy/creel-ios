// DYNAMIC FUNCTION TO CREATE TABLES
function selectTable(tableName, columns) {
    myDB.transaction(function(transaction) {
		// Create column arrays
		if(columns !== null && columns.length > 0){
			var columnNames = [];
			for (var i=0; i<columns.length; i++){
				columnNames[i] = columns[i];
			}		
			// Produce create query
			var columnNamesQuery = "";
			for (var i=0; i<columnNames.length-1; i++){
				if(i==0){
				    columnNamesQuery = columnNamesQuery + columnNames[i];
				}else{
				    columnNamesQuery = columnNamesQuery + ", " + columnNames[i];
				}
			}
			columnNamesQuery = columnNamesQuery + columnNames[columnNames.length-1];
		}else{
			columnNamesQuery = " * ";
		}
		// Create query
		var query= 'SELECT ' + columnNamesQuery + ' FROM ' + tableName;
        transaction.executeSql(query, [],
        function(tx, result) {
            var tableData = []
            for(var i=0; i<result.rows.length; i++){
                var row = {};
                row = result.rows.item(i);
                tableData.push(row);
            }
            return tableData;
        },
        function(error) {
            //alert("Error occurred while selecting table:" + JSON.stringify(error));
        });
    });
}

function selectTableProjects(columnNames) {
	selectTable("tblprojects",columnNames);
}

function selectTableSchedule(columnNames) {
	selectTable("tblschedule",columnNames);
}

function selectTableCountTimes(columnNames) {
	selectTable("tblcounttimes",columnNames);
}

function selectTableSpecies(columnNames) {
	selectTable("tblspecies",columnNames);
}

function selectTableProjectSpecies(columnNames) {
	selectTable("tblprojectspecies",columnNames);
}

function selectTableHeader(columnNames) {
	selectTable("tblheader",columnNames);
}

function selectTableCounts(columnNames) {
	selectTable("tblcounts",columnNames);
}

function selectTableParties(columnNames) {
	selectTable("tblparties",columnNames);
}

function selectTableAnglers(columnNames) {
	selectTable("tblanglers",columnNames);
}

function selectTableFishCaught(columnNames) {
	selectTable("tblfishcaught",columnNames);
}

function selectTableProjectsValidate(columnNames){
    selectTable("tblprojectsvalidate",columnNames);
}
