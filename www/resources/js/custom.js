/***** CUSTOM ALERT *****/
function navAlert(message){
    navigator.notification.alert(
        message,    // message
        onAlert,    // callback
        'Alert',    // title
        'OK'        // buttonName
    );
}

function onAlert(){
    // LEAVE BLANK
}

var functionName = "";

/******** BACK ********/
function back(target){
    navigator.notification.confirm(
        'Are you sure?',  // message
        function(buttonIndex){
            onBack(buttonIndex, target);
        },     // callback
        'Confirm',        // title
        ['OK','Cancel']   // buttonName
    );
}

function onBack(buttonIndex, target){
    if(buttonIndex == 1){
        if(target !== undefined){
            window.location = target;
        } else {
            window.history.back();
        }
    }
}

/******** CANCEL ********/
function cancel(target){
    navigator.notification.confirm(
        'Are you sure?',  // message
        function(buttonIndex){
            onCancel(buttonIndex, target);
        },         // callback
        'Confirm',            // title
        ['OK','Cancel']                  // buttonName
    );
}

function onCancel(buttonIndex, target){
    if(buttonIndex == 1){
        if(target !== undefined){
			if(target == "homepage.html"){
				window.localStorage.removeItem("practice");
			}
			window.location = target;
        } else {
            window.location = "homepage.html";
        }
    }
}

/******** REFUSE ********/
function refused(target){
    navigator.notification.confirm(
        'Are you sure you wish to refuse?',  // message
        function(buttonIndex){
            onRefuse(buttonIndex, target);
        },         // callback
        'Confirm',            // title
        ['OK','Cancel']                  // buttonName
    );
}

function onRefuse(buttonIndex, target){
    if(buttonIndex == 1){
        if(target !== undefined){
            window.location = target;
        }
    }
}

/******** FINISH ********/
function finish(target){
    navigator.notification.confirm(
        'Are you sure?',  // message
        function(buttonIndex){
            onFinish(buttonIndex, target);
        },         // callback
        'Confirm',            // title
        ['OK','Cancel']                  // buttonName
    );
}

function onFinish(buttonIndex, target){
    if(buttonIndex == 1){
        if(target !== undefined){
            window.location = target;
        }
    }
}


/******** LOGOUT ********/
function logout(){
    navigator.notification.confirm(
        'Are you sure you wish to logout?',  // message
        changeLogin,      // callback
        'Confirm',        // title
        ['OK','Cancel']   // buttonName
    );
}

function changeLogin(buttonIndex){
    if(buttonIndex == 1){
        window.location = "index.html";
    }
}

/********* RANDOM NUMBER GENERATOR *********/
function randomNumber(){
    var uuid = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    var uid = "";
    var hex = "0123456789ABCDEF";
    for(var i = 0; i < uuid.length; i++){
        var newChar = uuid[i];
        if(newChar == 'x'){
            newChar = hex.charAt(Math.floor(Math.random() * hex.length));
        }
        uid = uid + newChar;
    }
    return uid;
}

/********** CHECK WIFI CONNECTION ***************/
function checkConnection() {
	var networkState = navigator.connection.type;

	var states = {};
	states[Connection.UNKNOWN]  = 'Unknown connection';
	states[Connection.ETHERNET] = 'Ethernet connection';
	states[Connection.WIFI]     = 'WiFi connection';
	states[Connection.CELL_2G]  = 'Cell 2G connection';
	states[Connection.CELL_3G]  = 'Cell 3G connection';
	states[Connection.CELL_4G]  = 'Cell 4G connection';
	states[Connection.CELL]     = 'Cell generic connection';
	states[Connection.NONE]     = 'No network connection';

	return states[networkState];
}
