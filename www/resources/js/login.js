/** On Click of Login Button **/
function login(){
    window.localStorage.removeItem("practice");
    var projID = $("#selectProject option:selected").attr("id");
    if(projID == "na"){
        //window.location = "homepage.html";
        navigator.notification.alert(
            'Select a valid project',   // message
            '',                         // callback
            'Confirm',                  // title
            'OK'                        // buttonName
        );
    } else {
        var selectedProject = $("#selectProject option:selected").text();
        var projectUID = $("#selectProject option:selected").attr("id");
        var projType = $("#selectProject option:selected").attr("creel-type");
        window.localStorage.setItem("currentProject",selectedProject);
        window.localStorage.setItem("projectUID",projectUID.toString());
        window.localStorage.setItem("projectType",projType);

        // Check if dropdown or input type
        var select = $(".selectUser").hasClass('hidden');
        var input = $(".userInput").hasClass('hidden');
        if(input){
            //if(usernameList != null && usernameList.length>0){
            var selectedUser = $("#selectUser option:selected").text();
            var selectedUserID = $("#selectUser option:selected").attr("value");
            if(selectedUserID == "000"){
                navigator.notification.alert(
                    'Select a valid user',      // message
                    '',                         // callback
                    'Confirm',                  // title
                    'OK'                        // buttonName
                );
            } else {
                window.localStorage.setItem("currentUser",selectedUser);
                window.location = 'homepage.html';
            }
        } else { // (!input)
            inputUser = document.getElementById('username').value;
            inputUser = $.trim(inputUser);

            // Add new user to localStorage, if does not exist
            if($.trim(inputUser) != ""){

                // Parse usernameList to array of usernames
                var usernameArray = JSON.parse(usernameList);
                if(usernameArray != null){
                    var matched = false;
                    for (var i = 0; i<usernameArray.length; i++){
                        if(inputUser == usernameArray[i] && !matched){
                            matched = true;
                        } // end if-else (username!=null)
                    } // end for loop
                    if(matched){
                        navigator.notification.alert(
                            'Username already exists! Press sign in with existing account!',  // message
                            '',                         // callback
                            'Confirm',                  // title
                            'OK'                        // buttonName
                        );
                    } else{
                        usernameArray.push(inputUser);
                        window.localStorage.setItem("usernameList", JSON.stringify(usernameArray));
                        window.localStorage.setItem("currentUser", inputUser);
                        window.location = 'homepage.html';
                    }
                } else { // create new username list
                    var usernameArray = [];
                    usernameArray.push(inputUser);
                    window.localStorage.setItem("currentUser", inputUser);
                    window.localStorage.setItem("usernameList", JSON.stringify(usernameArray));
                    window.location = 'homepage.html';
                } // end if-else (inputUser != "")
            }// if (inputUser)
            else {
                navigator.notification.alert(
                    'Enter username',  // message
                    '',                // callback
                    'Confirm',         // title
                    'OK'               // buttonName
                );
            }
        } // end if-else (usernameList)
    } // end if-else (projID == 999)
}
